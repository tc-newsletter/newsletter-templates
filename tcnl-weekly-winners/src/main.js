/* eslint-disable react/prop-types */
import * as React from 'react';
import { render, Text, View } from 'react-sketchapp';
import Profile from './components/Profile';
import Group from './components/Group';
import { css, withStyles } from './withStyles';
import { data } from './data/weekly-winner.dev';

const Page = ({ groups }) => (
  <View>
    {
      groups.map((group, i) => (
        <Group key={i} group={group} >
          {
            group.profiles && group.profiles.map( (user, j) => (
              <Profile user={user} rank={j+1} />
            ))
          }
        </Group>
      ))
    }
  </View>
);

export default () => {
  const dbGroups = data;

  render(<Page groups={dbGroups} />, context.document.currentPage());
};
