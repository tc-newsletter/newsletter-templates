/* eslint-disable react/prop-types */
import * as React from 'react';
import { Image, View, Text } from 'react-sketchapp';
import { css, withStyles } from '../withStyles';
import { handleColor, rankColor, rankColorText } from '../theme';

const Profile = ({ user, styles, rank }) => {
  const currentRankColor = rankColor[rank ? rank : 0];
  return (
  <View {...css(styles.container)}>
    <View {...css(styles.imgWrap, {borderColor: currentRankColor})}>
    <Image source={user.profileImageUrl} {...css(styles.avatar)} />
     <View {...css(styles.rankCircle, {backgroundColor: currentRankColor})}><Text {...css(styles.rank, {color: rankColorText[rank ? rank: 0]})}>{rank}</Text></View>
    </View>
    <View {...css(styles.titleWrapper)}>
      <Text {...css(styles.title, {'color': handleColor[user.handleColor ? user.handleColor: 'default']}) }>{user.displayName}</Text>
    </View>
  </View>
)};

export default withStyles(({ colors, fonts, spacing }) => ({
  container: {
    paddingHorizontal: spacing,
    paddingVertical: 0,
    width: 104,
    height: 118,
    marginRight: spacing,
  },
  avatar: {
    height: 72,
    width: 72,
    resizeMode: 'contain',
    borderRadius: 36,
  },
  imgWrap: {
    height: 78,
    width: 78,
    borderRadius: 39,
    borderWidth: 3,
    position: 'relative'
  },
  rank: {
    height: 21,
    width: 21,
    fontFamily: 'Roboto', 
    fontWeight: '800',
    fontSize: 15,
    textAlign: 'center',
    lineHeight: 20
  },
  rankCircle: {
    position: 'absolute',
    right: -2,
    bottom: -2,
    height: 25,
    width: 25,
    borderRadius: 13,
    borderWidth: 2,
    borderColor: '#ffffff'
  },
  titleWrapper: {
    marginBottom: 30,
  },
  title: { 
    ...fonts['Title 2'],
    textAlign: 'center',
    color: handleColor.default 
  },
  subtitle: { ...fonts['Title 3'] },
  body: { ...fonts.Body },
}))(Profile);
