/* eslint-disable react/prop-types */
import * as React from 'react';
import { Image, View, Text } from 'react-sketchapp';
import { css, withStyles } from '../withStyles';

const Title = withStyles(({ fonts }) => ({
  titleText: fonts['Headline'],
}))(({ children, styles }) => <Text {...css(styles.titleText)}>{children}</Text>);

const Group = ({ children, group, styles }) => (
  <View>
    <Title>{group.title}</Title>
    <View style={{
      flexDirection: 'row',
      flexWrap: 'wrap',
      width: group.profiles.length * 400,
    }}>
      {children}
    </View>
  </View>
);

export default withStyles(({ colors, fonts, spacing }) => ({
  container: {
    backgroundColor: colors.Pear,
    padding: spacing,
    marginRight: spacing,
  }
}))(Group);
