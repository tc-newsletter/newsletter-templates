export const colors = {
  Haus: '#F3F4F4',
  Night: '#333',
  Sur: '#96DBE4',
  'Sur a11y': '#24828F',
  Peach: '#EFADA0',
  'Peach a11y': '#E37059',
  Pear: '#93DAAB',
  'Pear a11y': '#2E854B',
};

const typeSizes = [80, 48, 36, 24, 20, 16];

export const spacing = 16;
export const handleColor = {
  default: '#2A2A2B', 
  red: '#ef3a3a', 
  yellow: '#9a6d00',
  blue: '#5656f4',
  green: '#258025'
}
// rankcolor = default, gold, silver bronze
export const rankColor = ['#2A2A2B', '#FFD84D', '#D1D0CF', '#D98F64'];
export const rankColorText = ['#2A2A2B', '#865E07', '#554E48', '#744117'];


const fontFamilies = {
  display: 'Roboto',
  body: 'Georgia',
};

const fontWeights = {
  regular: 'regular',
  medium: 'medium',
  bold: 'bold',
};

export const fonts = {
  Headline: {
    color: "#0B71E6",
    fontSize: 15,
    fontFamily: fontFamilies.display,
    fontWeight: 'bold',
    lineHeight: 48,
  },
  'Title 1': {
    color: colors.Night,
    fontSize: typeSizes[2],
    fontFamily: fontFamilies.display,
    fontWeight: fontWeights.bold,
    lineHeight: 48,
  },
  'Title 2': {
    color: '#2A2A2B',
    fontSize: 13,
    fontFamily: fontFamilies.display,
    fontWeight: fontWeights.bold,
    lineHeight: 30,
  },
  'Title 3': {
    color: colors.Night,
    fontSize: typeSizes[4],
    fontFamily: fontFamilies.body,
    fontWeight: fontWeights.medium,
    lineHeight: 24,
  },
  Body: {
    color: colors.Night,
    fontSize: typeSizes[5],
    fontFamily: fontFamilies.body,
    fontWeight: fontWeights.regular,
    lineHeight: 24,
    marginBottom: 24,
  },
};

export default {
  colors,
  fonts,
  spacing,
};
