module.exports.data =[
    {
      "title": "Plant vs Zombies Design Challenge",
      "profiles": [
        {
          "displayName": "DaraK",
          "profileImageUrl": "https://www.topcoder.com/i/m/DaraK.png"
        },
        {
          "displayName": "PereViki",
          "profileImageUrl": "https://topcoder-prod-media.s3.amazonaws.com/member/profile/PereViki-1569353542408.jpeg"
        },
        {
          "displayName": "Ravijune",
          "profileImageUrl": "https://topcoder-prod-media.s3.amazonaws.com/member/profile/Ravijune-1471983063227.jpeg"
        }
      ]
    },
    {
      "title": "Plant vs Zombies Dev Challenge",
      "profiles": [
        {
          "displayName": "hi4sandy",
          "handleColor": "yellow",
          "profileImageUrl": "https://topcoder-prod-media.s3.amazonaws.com/member/profile/hi4sandy-1567271548674.png"
        },
        {
          "displayName": "jiangliwu",
          "handleColor": "blue",
          "profileImageUrl": "https://topcoder-prod-media.s3.amazonaws.com/member/profile/jiangliwu-1509503013837.jpeg"
        },
        {
          "displayName": "moulyg",
          "handleColor": "green",
          "profileImageUrl": "https://topcoder-prod-media.s3.amazonaws.com/member/profile/moulyg-1509873943377.jpeg"
        }
      ]
    },
    {
      "title": "Plant vs Zombies Dev Challenge 2",
      "profiles": [
        {
          "displayName": "hi4sandy",
          "handleColor": "yellow",
          "profileImageUrl": "https://topcoder-prod-media.s3.amazonaws.com/member/profile/hi4sandy-1567271548674.png"
        },
        {
          "displayName": "jiangliwu",
          "handleColor": "blue",
          "profileImageUrl": "https://topcoder-prod-media.s3.amazonaws.com/member/profile/jiangliwu-1509503013837.jpeg"
        }
      ]
    }
  ]