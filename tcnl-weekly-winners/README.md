# tcnl-weekly-winners

## How to use

Install the dependencies

```
npm install
```

Then, open Sketch and navigate to `Plugins → react-sketchapp: Profile Cards`

Run with live reloading in Sketch

```
npm run render
```

### Updating data
The winner data for different tracks can be updated by updating the files here:
- [design](./src/data/weekly-winner.design.js)
- [dev](./src/data/weekly-winner.dev.js)
- [data science](./src/data/weekly-winner.dev.js)

## Exmaple

![examples-profile-cards](https://gitlab.com/tc-newsletter/newsletter-templates/raw/master/tcnl-weekly-winners/assets/View.jpg)
