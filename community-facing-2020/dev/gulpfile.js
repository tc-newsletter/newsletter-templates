var project = "app";

//gulp builder
var gulp = require('gulp'),
  bowerFiles = require('main-bower-files'),
  inject = require('gulp-inject'),
  stylus = require('gulp-stylus'),
  concat = require('gulp-concat'),
  angularFilesort = require('gulp-angular-filesort'),
  es = require('event-stream'),
  clean = require('gulp-clean'),
  runSequence = require('run-sequence');
  inlineCss = require('gulp-inline-css');

sass = require("gulp-sass"),
  autoprefixer = require('gulp-autoprefixer'),
  sourcemaps = require('gulp-sourcemaps'),
  browserSync = require('browser-sync').create();



var srcPath = project + "/sass/**/*.scss";
var targetPath = project + "/css";




gulp.task('build', function() {
  runSequence(
    ['clean'], ['builder']
  );
})


//////////
// task //
//////////

gulp.task('inliner', function() {
    return gulp.src('./app/*.html')
        .pipe(inlineCss({
	        	applyStyleTags: false,
	        	applyLinkTags: true,
	        	removeStyleTags: false,
	        	removeLinkTags: true
        }))
        .pipe(gulp.dest('build/'));
});

// sass task
gulp.task("sass", function() {
  gulp.src(srcPath)
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: 'nested' }).on('error', sass.logError))
    .pipe(autoprefixer("ie 11", "last 3 version"))
    .pipe(sourcemaps.write('./map'))
    .pipe(gulp.dest(targetPath));
});
//serve app
gulp.task('serve', function() {

  browserSync.init({
    server: project,
    port: 4000
  });

  gulp.watch(srcPath, ["sass"]);
  gulp.watch(project + "/**/*.html").on('change', browserSync.reload);
});


// watch
gulp.task("watch", function() {
  gulp.watch(srcPath, ["sass"]);
});

gulp.task("default", ["serve", "watch"]);
