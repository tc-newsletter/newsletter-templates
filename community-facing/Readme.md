# community-facing-newsletter-template-design-and-code

## Bookmarks
Frequently used links
>- [Banners](https://photos.google.com/u/1/share/AF1QipPRoWpn1Q8YnkbrNMh7vvRLeUoWQ5qnye_i1UCLocwyT8B7aG6MmisSRmzuAV8bOA?key=VnByVTBMa3h3RlBzV1BNNDF3azV0VTFBN01HUF9n)
>- [Topcoder Newsletter Coummunity Slack Channel](https://topcodercommunity.slack.com/messages/G9JQNDU5N/)
>- [**Guidelines for creating Newsletter** (Google docs)](https://docs.google.com/document/d/1OKpio6xC1WL2lAHByX7NCNe6ZDpzOqQha5DoP6NXA_M/edit)
>- [community-facing-template-design-and-code (Google docs)](https://docs.google.com/document/d/1MaxbXabgRezdBYQYPohaBn9AG8Iw4oOcBOI1de3xu5s/edit#heading=h.6n586k251s1g)
>- [**Newsletter Banner Designs** (Googel drive)](https://drive.google.com/drive/folders/1KzgXzIDZ-TS5JwKWtlhi8Um0yWWX0Gy9)
>- [Markdown format keywords (Gitlab)](https://gitlab.com/help/user/markdown)
>- [Request new enhacement, feature or template](https://gitlab.com/tc-newsletter/newsletter-templates/issues/new)
>- [Report bugs 🐞](https://gitlab.com/tc-newsletter/newsletter-templates/issues/new)


