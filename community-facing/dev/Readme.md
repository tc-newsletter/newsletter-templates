# connect-email-template

## Overview
Using this submission email template can be generated. The generated templated can be sent to any desired emails using an email client. 


## Prerequisites
  - node 6.13+
  - Emails client to send & test emails
  
## Setting up the enviroment.
Run `$ cd <full submissions path>` to browse to the submission folder using commond promnt or Terminal
Include necesarry libraries and frameworks by running `$ npm run install` . Once all the libraries are downloaded run `$ gulp` to test the submission in browser. It host the submission on port `4000` the url to this is [](http://localhost:4000)

## Generating the email template
The email template can be generate by running following command: 
```
$ gulp inliner
```
The template build is generated in `build/index.html`

## Sending generated template to an email account.
Open the file `<path to submssion>/build/index.html` in any editor & copy the entire code, after this visit [putsmail](https://putsmail.com/tests/new) and paste the copied code into the provided textarea, specify the mail id & enter other necessary details. Click _Send Email_ to send the mail.

Once mail has been sent. Check the email account in browser or email client for verification. Test in both mobile & desktop pc to verify responsiveness.
![Gmail](https://cdn-std.dprcdn.net/files/acc_652531/WnKE6O)
![Gmail Mobile](https://cdn-std.dprcdn.net/files/acc_652531/1M6BXb)

Thank you
